The Middlebury Language Schools offer intensive undergraduate and graduate-level instruction in 11 languages during six-, seven-, or eight-week summer sessions.

Website: https://www.middlebury.edu/language-schools/
